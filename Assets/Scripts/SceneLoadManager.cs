using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneLoadManager:MonoBehaviour
{
    public static SceneLoadManager instance { get; private set; }
    
    [SerializeField]
    private GameObject canvasContainer;
    [SerializeField]
    private Image fader;
    [SerializeField]
    private float fadeInSpeed = 1.0f;
    [SerializeField]
    private float fadeOutSpeed = 2.0f;

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }  
    }

    public void loadScene(string sceneName)
    {
        StartCoroutine(fadeAndLoadScene(sceneName));
    }

    private IEnumerator fadeAndLoadScene(string sceneToLoad)
    {
        canvasContainer.SetActive(true);

        yield return StartCoroutine(fadeIn());
        SceneManager.LoadScene(sceneToLoad);
        yield return null;
        yield return StartCoroutine(fadeOut());

        canvasContainer.SetActive(false);
    }

    private IEnumerator fadeIn()
    {
        float alpha = 0.0f;
        var color = fader.color;
        color.a = alpha;
        fader.color = color;

        while(color.a < 1)
        {
            yield return null;

            alpha += fadeInSpeed * Time.deltaTime;
            color.a = alpha;
            fader.color = color;
        }

        color.a = 1.0f;
        fader.color = color;
    }

    private IEnumerator fadeOut()
    {
        float alpha = 1.0f;
        var color = fader.color;
        color.a = alpha;
        fader.color = color;

        while (color.a > 0)
        {
            yield return null;

            alpha -= fadeOutSpeed * Time.deltaTime;
            color.a = alpha;
            fader.color = color;
        }

        color.a = 0.0f;
        fader.color = color;
    }
}