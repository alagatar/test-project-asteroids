using UnityEngine;

public class PlayerMovementController:MonoBehaviour
{
    [SerializeField]
    private float moveSpeed = 1f;
    private Camera cam;
    
    private void Awake()
    {
        cam = Camera.main;
    }

    private void Update()
    {
        lookToMouse();
        processMove();
    }

    private void lookToMouse()
    {
        Vector3 mouseOnScreen = cam.ScreenToWorldPoint(Input.mousePosition);
        Vector3 direction = transform.position - mouseOnScreen;
        Quaternion newRotation = Quaternion.LookRotation(direction, Vector3.forward);
        transform.rotation = newRotation;
    }

    private void processMove()
    {
        float moveV = Input.GetAxis("Vertical") * moveSpeed;
        float moveH = Input.GetAxis("Horizontal") * moveSpeed;
        Vector3 moveVelocity = (transform.up * moveV) + (transform.right * moveH);
        transform.position += moveVelocity * Time.deltaTime;
    }
}